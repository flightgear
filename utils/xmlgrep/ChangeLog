21-07-2008
  * change a number of function parameters to const where appropriate
  * fix a problem where the wrong node-name length was returned
  * xmlgrep now also works when only the -e options is specified
  * fix xmlgrep to show the correct node-name (it reported the parent
    node-name in the previous version)

20-07-2008
  * fix __xmlSkipComment to properly find the end of comment tag.
  * add the xmlGetNodeName and xmlCopyNodeName functions
  * add the xmlCopyString function
  * clean up some code

19-07-2008
  * rewrite the code to always recursively walk the node tree when searching
    for a particular node. this is required for cases where a node with a
    particular name is located deeper in a node with the same name;
    for example -r /configuration/device/reference/device would fail in the 
    previous version
  * rename xmlGetElement to xmlGetNodeNum and add the possibility to request
    the nth node with this name
  * rename xmlGetNumElements to xmlGetNumNodes

06-07-2008
  * reorganize the code to be able to skip comment sections
  * depreciate __xmlFindNextElement and use __xmlGetNode instead
  * xmlGetNextElement now returns char* instead of void* for future use
  * add preliminary support for wildcards in the search path ('*' and '?')

01-07-2008
 * fix a problem caused by removing the last unnecessary alloc
 * strip leading-, and trailing spaces from the string before comparing
 * fix a problem where trailing spaces weren't removed

30-06-2008:
 * some small changes; fix some typo's and fix a small memory leak
 * update the documentation in README
 * remove the last unnecessary alloc

29-06-2008:
 * rename xmlGet(Int/Double/String) to xmlGetNode(Int/Double/String)
 * add new xmlGet(Int/Double/String) functions
 * rename xmlCompareString to xmlCompareNodeString for consistency
 * rename xmlCompareElement to xmlCompareString for consistency
 * add a README file with short examples of various functions

27-06-2008:
 * removed some memory allocation in xmlGetNode and XMLGetNextElement
 * use the file-size for mmap and remove the root node from the xml-id
 * rearrange xmlGetNode to work with complicated xml files
 * add the xmlMarkId function to save the id before using xmlGetNextElement
 * speed up xmlGetNextId

23-06-2008: Initial release
